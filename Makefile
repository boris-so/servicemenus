SCRIPTS_DST="/usr/local/src"
DESKTOP_FILES_DST="/usr/share/kservices5/ServiceMenus/"

install_systemwide: install_scripts install_desktops

install_scripts:
	cp -v scripts/*.sh $(SCRIPTS_DST)

install_desktops:
	sudo cp -v desktop_files/*.desktop $(DESKTOP_FILES_DST)


clean:
	@echo "No automatic cleanup yet!"
	@echo "Remove files manually from this locations:"
	@echo "$(SCRIPTS_DST)"
	@echo "$(DESKTOP_FILES_DST)"