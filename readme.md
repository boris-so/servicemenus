
### Basic info

A set of files providing some root actions for Dolphin file browser.

All files are of two types:

-  bash scripts, actually performing actions
-  `.desktop-files`, called "service menus" in KDE terminology

### Files included

#### Scripts
All scripts are based on `kdialog` set of gui elements and `pkexec` is used for privelege escalation. Also other basic utilities are used. such as `wc`, `find` and others. Look for script files to find them out.

#### Desktop files
[See this kde tutorial](https://develop.kde.org/docs/dolphin/service-menus/) to get more information about custom entries in dolphin\`s context menu.

In short -`.desktop` files (formatted in special way) provide ability to make custom centext menu entries for dolphin (and maybe some other file browsers).

### Installation
If u are using KDE5, place `.desktop` files under `~/.local/share/kservices5/ServiceMenus/` or `/usr/share/kservices5/ServiceMenus/`, and script files under `/usr/local/src`

Or simply run `make` for system-wide installation.

### Categories
#### Root actions
General actions used with root priveleges:

	- touch
	- chown
	- chmod
	- rm
	
#### unzip
Suitable for zip-archives created on windows systems with cyrillic codepage.
Shortcut for commands `unzip -UU` and `convmv -f latin1 -t cp850 --notest *; convmv -f cp866 -t utf8 --notest`.

#### vsftpd
Robust script allows to share directories for annonymous acces via ftp.
A folder and a user must be existing to let vsftpd start correctly:
	- secure_chroot_dir=/var/run/vsftpd/empty
	- nopriv_user=ftp

#### GraphicsMagick
Join many `tif` files into single multipage file