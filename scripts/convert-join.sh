#!/bin/bash

IFS="
"

FILES_LST=$(echo "$@" | sed 's/ \//\n\//g')
FIRST_FILE=$(echo "$FILES_LST" | head -n 1)
DEFAULT_DIR=$(dirname $FIRST_FILE)

OFNAME=$(kdialog --inputbox "Имя файла для слияния (расширение tif будет выбрано автоматически)" "$(basename $FIRST_FILE)")

# Exit, if cancell
if test $? -eq 1; then exit 1; fi


# Check user input either absolute or relative path
case "$OFNAME" in
     /*) OFPATH="$OFNAME" ;;
     *) OFPATH="$DEFAULT_DIR/$OFNAME" ;;
esac

RES=$(convert $FILES_LST -adjoin "$OFPATH.tif" 2>&1)
if test $? -ne 0; then
	kdialog --error "$RES"
	exit 1
fi
exit 0