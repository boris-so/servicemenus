# See file "pkexec_chown.sh" for comments.
FILES_LST=$(echo "$@" | sed 's/ \//\n\//g')
# Field separator - newline, because we have lines of paths, which may contain spaces.
IFS="
"
# Run gui window with message. Show file names in per-line style
MOD=$(kdialog --inputbox "Смена режима доступа для следующих файлов/директорий:

$FILES_LST

(используйте синтаксис chmod)")
test $? -eq 0 || exit 2;
MOD=$(echo -n "$MOD" | tr " " "\n")
NON_EXISTENT=""
for f in $FILES_LST; do
	test -e $f || NON_EXISTENT="$NON_EXISTENT\n$f"
done

if [[ -z $NON_EXISTENT ]]; then
	RES=$(pkexec chmod -v $MOD $FILES_LST 2>&1)
	if [[ $? -ne 0 ]]; then
		kdialog --error "Команда не выполнена:

$RES"
		exit 1
	fi
	kdialog --msgbox "Операция выполнена успешно:
$RES"
else
	kdialog --error "Следующие файлы/директории не существуют:

$NON_EXISTENT

Команда не выполнена."
	exit 1
fi