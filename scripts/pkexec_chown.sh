# We expect absolute paths only. So replacing " /" with "\n" would give us
# list of absolute paths, one per line (including spaces in paths)
FILES_LST=$(echo "$@" | sed 's/ \//\n\//g')
# Field separator - newline, because we have lines of paths, which may contain spaces.
# DO NOT USE QUOTES FOR $FILES_LST AND SIMILAR VARIABLES!
# In that case they will be treated as single value, not multiple!
IFS="
"
# Run gui window with message. Show file names in per-line style
OWNER=$(kdialog --inputbox "Смена владельца для следующих файлов/директорий:

$FILES_LST

(используйте синтаксис chown)" "boris:boris")
test $? -eq 0 || exit 2;
OWNER=$(echo -n "$OWNER" | tr " " "\n")

# As soon we are using \n as fileld separator, arg list must be also newline-formatted.
# Owner is not only user:group. Another keys allowed, such as "-R root:root"

# Exit on cancell

# Check all files/dirs exist
NON_EXISTENT=""
for f in $FILES_LST; do
	test -e $f || NON_EXISTENT="$NON_EXISTENT\n$f"
done

if [[ -z $NON_EXISTENT ]]; then
	# Despite of IFS is newline here, this one-line command works good.
	RES=$(pkexec chown -v $OWNER $FILES_LST 2>&1)
	# Check result
	if [[ $? -ne 0 ]]; then
		kdialog --error "Команда не выполнена:

$RES"
		exit 1
	fi
	kdialog --msgbox "Операция выполнена успешно:
$RES"
else
	# or show non-existent files
	kdialog --error "Следующие файлы/директории не существуют:

$NON_EXISTENT

Команда не выполнена."
	exit 1
fi