CUR_DIR="$1"
NEW_DIR=$(kdialog --inputbox "Имя каталога:" "Новый каталог")

test $? -eq 0 || exit 1
if ! echo -n "$NEW_DIR" | grep -P "^[а-яА-Яa-zA-Z0-9_./ -]+\$"; then
    kdialog --error "Использованы недопустимые символы.
Невозможно создать каталог с именем \"$NEW_DIR\""
    exit 1
fi

pkexec mkdir --parents "$CUR_DIR/$NEW_DIR"
exit 0
