# See file "pkexec_chown.sh" for comments.
FILES_LST=$(echo "$@" | sed 's/ \//\n\//g')
IFS="
"
kdialog --yesno "Вы собираетесь удалить следующие файлы/каталоги (включая содержимое):

$FILES_LST

Подтвердите."
test $? -eq 0 || exit 2;

RES=$(pkexec rm -r -v $FILES_LST 2>&1)
if [[ "$?" -ne 0 ]]; then
	kdialog --error "Некоторые файлы/каталоги не были удалены:

$RES"
	exit 1
fi
kdialog --msgbox "Выполнено:

$RES"
exit 0
