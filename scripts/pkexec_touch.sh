CUR_DIR="$1"
FN=$(kdialog --inputbox "Имя файла:" "file.txt")

test $? -eq 0 || exit 1
if ! echo -n "$FN" | grep -P "^[а-яА-Яa-zA-Z0-9_. -]+\$"; then
    kdialog --error "Использованы недопустимые символы.
Невозможно создать файл с именем \"$FN\""
    exit 1
fi

pkexec touch "$CUR_DIR/$FN"
exit 0
