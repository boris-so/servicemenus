#!/bin/bash

if ss -ltn | grep -o -q ":21"; then
	kdialog --error "Порт 21 занят другим процессом. Нужно завершить занимающие этот порт задачи."
	exit 1
fi
DIR="$1"

if ! test -e "$DIR"; then
	kdialog --error "Каталог $DIR не существует"
	exit 2
fi

kdialog --yesno "Сделать каталог $DIR доступным анонимно по FTP?"
test $? -eq 0 || exit 3;

VSFTPD_DIR=$(mktemp -d --suffix .vsftpd)
CONF="$VSFTPD_DIR/vsftpd.conf"
# Configuration file for vsftpd
tee "$CONF" << EOF > /dev/null
listen=YES
listen_ipv6=NO

anon_root=$DIR
anonymous_enable=YES
anon_upload_enable=YES
anon_mkdir_write_enable=YES
anon_other_write_enable=YES
write_enable=YES
local_enable=NO

dirmessage_enable=YES
use_localtime=YES
xferlog_enable=YES
syslog_enable=YES
connect_from_port_20=YES
nopriv_user=ftp
secure_chroot_dir=/var/run/vsftpd/empty
pam_service_name=vsftpd
ssl_enable=NO
utf8_filesystem=YES

EOF

# Separate script runs vsftpd in background. Would be run with root priveleges!
# Owner for config file must be root.
echo $VSFTPD_DIR
PIDFILE="/var/run/vsftpd.pid"
SCRIPT="$VSFTPD_DIR/run_vsftpd.sh"
tee "$SCRIPT" << EOF > /dev/null
chown root: $CONF
vsftpd $CONF &
echo \$! > $PIDFILE
EOF

pkexec bash $SCRIPT
sleep 0.5s
if pgrep vsftpd; then
	ip=$(ip addr show | grep "inet " | tail -n +2 | cut -d" " -f6 | cut -d/ -f1)
	url=""
	for i in $ip; do
		url="$url
ftp://$i:21"
	done
	kdialog --msgbox "Демон vsftpd запущен.
	$url

После закрытия этого окна он будет остановлен."
	pkexec bash -c "pkill -SIGTERM -F $PIDFILE && rm -fv $PIDFILE $CONF $SCRIPT"
	rmdir -v "$VSFTPD_DIR"
else
	kdialog --error "Ошибка при запуске vsftpd. Нужна дополнительная отладка.
	$VSFTPD_DIR"
	exit 3
fi
