# Extracts files with unzip -UU and apply file name conversion

ARCHIVE="$1"
IFS="
"
# Extract into empty dirs only!
while "true"; do
	DEST_DIR=$(kdialog --getexistingdirectory --title "Распаковать в")
	# Exit on cancell
	test $? -eq 0 || exit 2;
	if test -z "$(ls -A $DEST_DIR)"; then
		break
	else
		kdialog --yesno "Директория не пустая. Выбрать другую?" || exit 2
	fi
done

# Check if archive is encrypted
PWD=""
if 7z l -slt $ARCHIVE | grep -i -q -E "encrypted.+\+"; then
	PWD=$(kdialog --password "Пароль к архиву")
	test $? -eq 0 || exit 2;
fi
if test -n "$PWD"; then
	RES=$(unzip -P $PWD -UU -d $DEST_DIR $ARCHIVE 2>&1)
else
	RES=$(unzip -UU -d $DEST_DIR $ARCHIVE 2>&1)
fi
# Check for unzip return code
if test $? -ne 0; then
	kdialog --error "Ошибки во время распаковки:

$RES"
	exit 1
fi

# Run convmv with wildcards to get rid of issues with
# symbols when passing them as arguments
for d in $(find $DEST_DIR -type d); do
 	ls $d/*
  	convmv -f latin1 -t cp850 --notest $d/*
  	convmv -f cp866 -t utf8 --notest $d/*
done

# If file count more than 10 - display dots
FILES=$(ls -NAx "$DEST_DIR" | head -n 10 )
if test $(ls -NAx "$DEST_DIR" | wc -l) -gt 10; then
	FILES="$FILES
	..."
fi

kdialog --msgbox "Распаковка выполнена успешно:

$FILES"